from flask import Flask, render_template, request
from dotenv import load_dotenv
from pathlib import Path
import json
import os
import mysql.connector

app = Flask(__name__)

load_dotenv()
env_path = Path('.')/'.env'
load_dotenv(dotenv_path=env_path)

DBHost = os.getenv("DBHOST")
DBName = os.getenv("DBNAME")
DBUser = os.getenv("DBUSER")
DBPass = os.getenv("DBPASS")


@app.route("/")
def home():
    return render_template('index.html')

@app.route("/fetchpage")
def fetchpage():
    return render_template('fetch.html')

@app.route("/registerpage")
def registerpage():
    return render_template('register.html')

@app.route('/createtable')
def createtable():
        try:
            mydb = mysql.connector.connect(
                host=DBHost,
                user=DBUser,
                password=DBPass,
                database=DBName
            )
        except:
            return("<h4>DB Connection error. Please check your credentials<h4> <a href='/'>home</a>")
        mycursor = mydb.cursor()
        #check if table already exists
        mycursor.execute("SHOW TABLES")
        tables = mycursor.fetchall()
        for (table_name,) in tables:
            if table_name == "students":
                return ("<h4>Table already exists. You can start to register new students<h4> <a href='/'>home</a>") 

        #if table doesnot exist, create one        
        mycursor.execute("CREATE TABLE students (firstname VARCHAR(255), lastname VARCHAR(255))")
        return ("<h4>Table created sucessfully. You can start to register new students<h4> <a href='/'>home</a>")

@app.route('/fetchstudents', methods = ['GET'])
def fetch():
    flag = False
    if request.method == 'GET':
        try:
            mydb = mysql.connector.connect(
                host=DBHost,
                user=DBUser,
                password=DBPass,
                database=DBName
            )
        except:
            return("<h4>DB Connection error. Please check your credentials<h4> <a href='/'>home</a>")
        mycursor = mydb.cursor()
        #check if table exists
        mycursor.execute("SHOW TABLES")
        tables = mycursor.fetchall()
        for (table_name,) in tables:
            if table_name == "students":
                flag = True
                break
        if flag == False:
            mydb.close()
            return ("<h4>No DB table found. Create a table first.<h4> <a href='/'>home</a>")
        else:
            mycursor.execute("SELECT * FROM students")
            row_headers=[x[0] for x in mycursor.description] 
            rv = mycursor.fetchall()
            mydb.close()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return render_template('fetch.html', students=json_data)


@app.route('/registerstudents', methods = ['POST'])
def register():
    if request.method == 'POST':
        flag = False
        try:
            mydb = mysql.connector.connect(
                host=DBHost,
                user=DBUser,
                password=DBPass,
                database=DBName
            )
        except:
            return("<h4>DB Connection error. Please check your credentials<h4> <a href='/'>home</a>")
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        mycursor = mydb.cursor()
        #check if table exists
        mycursor.execute("SHOW TABLES")
        tables = mycursor.fetchall()
        for (table_name,) in tables:
            if table_name == "students":
                flag = True
                break
        if flag == False:
            mydb.close()
            return ("<h4>No DB table found. Create a table first.<h4> <a href='/'>home</a>")
        else:
            sql = "INSERT INTO students (firstname, lastname) VALUES (%s, %s)"
            val = (firstname, lastname)
            mycursor.execute(sql, val)
            mydb.commit()
            mydb.close()
            return "<h4>Registered sucessfully!!<h4> <a href='/'>home</a> "


    app.run(host='0.0.0.0',  port=80)